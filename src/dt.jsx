import React from 'react';
import ReactDOM from 'react-dom';
import Diem from "./date.jsx";
import Tempus from "./time.jsx";


const divStyle = {
    border: '1px solid black',
    color: 'gray'
};


export default class DiemTempus extends React.Component {

    render() {
        return (<div>
            <form style={divStyle}>
                <p>I say wippit!</p>
                <p>wippit good!</p>
                <Tempus/>
                <Diem/>
            </form>
        </div>);
    }
}

ReactDOM.render(<DiemTempus/>, document.getElementById('hush-a-boom'));
