import React from 'react';
import ReactDOM from 'react-dom';

const divStyle = {
    border: '1px solid black',
    color: 'orange'
};

export default class Diem extends React.Component {
    render() {
        return (<div style={divStyle}>
            <p>{new Date().toLocaleDateString()}</p>
        </div>);
    }
}

ReactDOM.render(<Diem/>, document.getElementById('diem'));
