import React from 'react';
import ReactDOM from 'react-dom';

const headFootStyle = {
    backgroundColor: 'teal',
    color: 'white'
}

const divStyle = {
    border: '1px solid black',
    borderCollapse: 'collapse',
};

const tableStyle = {
    border: '1px solid black',
    color: 'black',
    borderCollapse: 'collapse',
    width: '100%'

};

const headerRowStyle = {
    border: '1px solid black',
    color: 'black',
    borderCollapse: 'collapse',
    backgroundColor: 'cadetblue'
};


const rowStyle = {
    border: '1px solid black',
    color: 'black',
    borderCollapse: 'collapse'

};

function HumanTimeDiff(d0, d1) {
    const d_0 = new Date(d0);
    const d_1 = new Date(d1);

    const dpw = 7;
    const hpd = 24;
    const mph = 60;
    const spm = 60;

    const sec_per_week = dpw * hpd * mph * spm;
    const sec_per_day = hpd * mph * spm;
    const sec_per_hour = mph * spm;
    const sec_per_minute = spm;

    let remaining_secs = Math.floor((d_1.getTime() - d_0.getTime())/1000);

    let weeks = Math.floor(remaining_secs / sec_per_week);
    remaining_secs = remaining_secs - weeks * sec_per_week;

    let days = Math.floor(remaining_secs / sec_per_day);
    remaining_secs = remaining_secs - days * sec_per_day;

    let hours = Math.floor(remaining_secs / sec_per_hour);
    remaining_secs = remaining_secs - hours * sec_per_hour;

    let minutes = Math.floor(remaining_secs / sec_per_minute);
    remaining_secs = remaining_secs - minutes * sec_per_minute;

    let seconds = remaining_secs;

    let rv = new String('');
    if (weeks > 0) {
        rv = rv + weeks.toString() + ' weeks, ';
    }
    if (days > 0) {
        rv = rv + days.toString() + ' days, ';
    }
    if (hours > 0) {
        rv = rv + hours.toString() + ' hours, ';
    }
    if (minutes > 0) {
        rv = rv + minutes.toString() + ' minutes, ';
    }
    if (seconds > 0) {
        rv = rv + seconds.toFixed(0).toString() + ' seconds.';
    } else {
        rv = rv.slice(0, -2) + '.';
    }
    return rv;

}

export default class S3List extends React.Component {

    constructor() {
        super();
        this.state = {items: []};
    }

    componentDidMount() {
        fetch(`http://localhost:5000/s3/ls`)
            .then(result => result.json())
            .then(items => this.setState({items}));
    }

    render() {
        const now = new Date();
        return (<div style={divStyle}>
            <div align='center' style={headFootStyle}><b>S3 Bucket Listing</b></div>
            <table style={tableStyle}>
                <tbody>
                <tr>
                    <th style={headerRowStyle}>Bucket Name</th>
                    <th style={headerRowStyle}>Creation Date</th>
                    <th style={headerRowStyle}>Age</th>
                </tr>
                {this.state.items.map(item => (
                    <tr>
                        <td style={rowStyle}>{item[0]}</td>
                        <td style={rowStyle}>{item[1]}</td>
                        <td style={rowStyle}>{HumanTimeDiff(item[1], now)}</td>
                    </tr>))}
                </tbody>
            </table>
            <div style={headFootStyle}>{this.state.items.length} items.</div>
        </div>);
    }
}

ReactDOM.render(<S3List/>, document.getElementById('s3list'));

