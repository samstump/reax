import React from 'react';
import ReactDOM from 'react-dom';

import Reactable from 'reactable';

let Table = Reactable.Table;
let Tr = Reactable.Tr;

let bipeds = [
        { Name: 'Griffin Smith', Resides: 'Austin, TX' },
        { Name: 'Peter Griffin', Resides: 'Quahog, RI' },
        { Name: 'Elephants Gerald', Resides: 'Savannah, GA' },
        { Name: 'Pikop Andropov', Resides: 'Caimbridge, MA' },
        { Name: 'Griffin Smith', Resides: 'Austin, TX' }
    ];


export default class BipedList extends React.Component {
    constructor() {
        super();
    }

    componentDidMount() {
        console.log('mounted');
    }


    renderTable() {
        return (
            <Reactable.Table className="table" data={bipeds} />
        );
    }

    render() {
        return (
            <div>
                {this.renderTable()}
            </div>
        );

    }
}
ReactDOM.render(<BipedList/>, document.getElementById('table'));
