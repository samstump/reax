import React from 'react';
import ReactDOM from 'react-dom';

const divStyle = {
    border: '1px solid black',
    color: 'green'
};

export default class Tempus extends React.Component {
    render() {
        return (<div style={divStyle}>
            <p>{new Date().toLocaleTimeString()}</p>
        </div>);
    }
}



ReactDOM.render(<Tempus/>, document.getElementById('tempus'));

