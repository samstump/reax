# reax

Simple demo of react/babbel/webpack with Python/Flask backend.

## installation
``git clone <repo>``

```npm install```


## starting the dev server
```node_modules/.bin/webpack-dev-server --watch```

## starting the backend server
```cd server```

```python3 server.py```

The API provided is essentially ```aws s3 ls``` a listing of the accessible s3 buckets.
You'll need to adjust credentials.

I have not packaged the backend dependencies.  The primary dependencies are:

- Flask
- boto3


I'd recommend a python3 virtual environment.  Instructions for that will be forthcoming.

## Notes
The ```src``` folder contains all of the react components. 
There are a number of components implemented but not included on the demo page, ```index.html```.

The sample components are:

- Time: simple time

- Date: simple date

- Datetime: Date plus Time

- Hello: static text example 1

- World: static text example 2

- BipedList: a list of humans

- S3List: a list of s3 bucket meta-data
