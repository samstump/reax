from flask import Flask, json
from flask import Response
from flask_cors import CORS
import datetime
from pytz import timezone
import boto3
import hashlib

app = Flask("data-server")
CORS(app)

@app.route("/s3/ls")
def s3_ls():
    session = boto3.Session(profile_name='reaper')
    client = session.client("s3")
    response = client.list_buckets()
    time_now = datetime.datetime.now(timezone('UTC'))
    data = response['Buckets']
    data = list(map(lambda t: (t['Name'], t['CreationDate'], (time_now - t['CreationDate']).total_seconds()), data))
    return Response(response=json.dumps(data), status=200,mimetype='application/json',content_type='application/json')

@app.route("/version")
def version():
    return "0.0.1"

@app.route("/ping")
def ping():
    return str(datetime.datetime.now(timezone('UTC')))

@app.route("/hash")
def hash_response():
    data = str(datetime.datetime.now(timezone('UTC')))
    m = hashlib.sha256()
    m.update(data.encode('utf-8'))
    return str(m.hexdigest())

app.run()
